package com.example;

public class Account {
    /*Account has facility to debit ,deposit and view the balance.Nitu and gitu are two accountholders who are sharing
    the account.Please help nitu and gitu to get the balance if other one debit or deposit
     */
    private static float balance;

    public static float getBalance() {
        return balance;
    }

    public static void setBalance(float balance) {
        Account.balance = balance;
    }

    public void debit(float amount){
        balance=balance-amount;
    }
    public static void  deposit(float amount){
        balance=balance+amount;
    }


}
